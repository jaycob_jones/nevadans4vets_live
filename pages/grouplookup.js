import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { Controller, useForm } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import url from "../utils/baseUrl";
const GroupLookUp = () => {
  const [errorMessage, setErrorMessage] = useState(false);

  const {
    control,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();
  const router = useRouter();
  const onSubmit = async (data) => {
    setErrorMessage(false);
    await axios
      .post(`${url}/api/findgolfer`, {
        data,
      })
      .then((res) => {
        setErrorMessage(false);

        router.push({
          pathname: "/[game]/players",
          query: { game: res.data.group_id },
        });
      })
      .catch((err) => {
        console.log(err);
        setErrorMessage(true);
      });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Container maxWidth="md">
        <Box my={4}>
          <Grid container spacing={3}>
            <Grid style={{ marginBottom: "50px" }} item xs={12}>
              <Typography variant="h6" component="h1">
                Please enter the email of the other player you would like to
                join.
              </Typography>
              <p>You will be able to join if the group is not full.</p>
            </Grid>
            <Grid item xs={12}>
              <Controller
                name={`email`}
                control={control}
                defaultValue=""
                rules={{ required: true }}
                render={({ field }) => (
                  <>
                    <TextField
                      error={errors[`email`] || errorMessage}
                      fullWidth
                      label="Email"
                      {...field}
                    />
                  </>
                )}
              />
            </Grid>
            <Grid item xs={12}>
              {errorMessage && (
                <p>
                  Sorry we cannot add you to this group. We did not find a
                  golfer found with this email. Please check the email and try
                  again.{" "}
                </p>
              )}
            </Grid>
            <Grid item xs={12}>
              <input type="submit" value="Join Group" />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </form>
  );
};

export default GroupLookUp;
