import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import url from "../utils/baseUrl";

const Registration = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onSubmit = async (data) => {
    setLoading(true);
    await axios
      .post(`${url}/api/creategroup`, {
        data,
      })
      .then((res) => {
        router.push({
          pathname: "/[game]/players",
          query: { game: res.data.group_id },
        });
      });
    setLoading(false);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Container maxWidth="md">
        <Box my={4}>
          <Grid container spacing={3}>
            <Grid style={{ marginBottom: "50px" }} item xs={12}>
              <Typography variant="h6" component="h1">
                Welcome to the event registration page
              </Typography>
              <p>Please select a game or add yourself to an existing group.</p>
            </Grid>
            <Grid item xs={12}>
              <InputLabel>Game</InputLabel>
              <Controller
                name="game"
                control={control}
                defaultValue="none"
                render={({ field }) => (
                  <Select fullWidth {...field}>
                    <MenuItem disabled={true} value="none">
                      <em>Select One</em>
                    </MenuItem>
                    <MenuItem value="2-person-better-ball">
                      2-Person Better Ball
                    </MenuItem>
                    <MenuItem value="4-person-scramble">
                      4-Person Scramble
                    </MenuItem>
                  </Select>
                )}
              />
            </Grid>
          </Grid>
        </Box>
        <input type="submit" value={loading ? "Loading..." : "Next Page"} />
      </Container>
      <Container maxWidth="md">
        <Box my={4}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography
                style={{ marginTop: "30px" }}
                variant="h6"
                component="h1"
              >
                Looking to join a preexisting group?{" "}
                <span
                  style={{ cursor: "pointer", textDecoration: "underline" }}
                  onClick={() => {
                    router.push({
                      pathname: "/grouplookup",
                    });
                  }}
                >
                  Click here
                </span>
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </form>
  );
};

export default Registration;
