import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { data } = req.body;
    await prisma.golfer
      .delete({
        where: {
          golfer_id: data.golfer_id,
        },
      })
      .then((results) => res.status(200).json(results));
  }
}
