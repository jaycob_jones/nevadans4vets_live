import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async function handler(req, res) {
  const { data } = req.body;
  if (req.method === "POST") {
    await prisma.golfer
      .findFirst({
        where: {
          email: data.email,
        },
      })
      .then((results) => {
        if (results) {
          res.status(200).json(results);
        } else {
          res.status(401).json({ message: "No user found." });
        }
      })
      .catch((err) => {
        res.status(401).json({ message: "No user found." });
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  }
}
