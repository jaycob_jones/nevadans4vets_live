import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async function handler(req, res) {
  if (req.method === "POST") {
    const {
      first_name,
      last_name,
      ghin,
      phone,
      email,
      tees,
      shirt_size,
      membership,
      group_id,
    } = req.body.data;

    await prisma.golfer
      .create({
        data: {
          first_name,
          last_name,
          ghin,
          phone,
          email,
          tees,
          shirt_size,
          membership,
          group_id: parseInt(group_id, 10),
        },
      })
      .then((results) => {
        res.status(200).json(results);
      })
      .catch((err) => {
        res.status(400).json({ error: "email already in use" });
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  }
}
