import { PrismaClient } from "@prisma/client";

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { data } = req.body;

    const prisma = new PrismaClient();

    await Promise.all(
      data.golfers_id.map(async (id) => {
        await prisma.golfer
          .update({
            where: {
              golfer_id: parseInt(id, 10),
            },
            data: {
              transaction_id: data.payment_id,
              paid: true,
            },
          })

          .catch((err) => conosle.log(err));

        return [];
      })
    ).catch((err) => conosle.log(err));

    await prisma.$disconnect();

    res.status(200).json({
      group_id: data.group_id,
      payment_id: data.payment_id,
      price_paid: data.price_paid,
      // group_id: "test",
      // payment_id: "test",
      // price_paid: "test",
    });
  }
}
