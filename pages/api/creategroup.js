import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { data } = req.body;
    await prisma.groups
      .create({
        data: {
          game_type: data.game,
        },
      })
      .then((results) => {
        res.status(200).json(results);
      })
      .catch((error) => {
        console.log(error);
        res.status(400);
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  }
}
