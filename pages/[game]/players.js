import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import url from "../../utils/baseUrl";

const calculatePrice = (golfers) => {
  let price = 0;
  golfers.map((x) => {
    if (!x.paid) {
      if (x.membership) {
        if (x.membership === "member") {
          price += 125;
        } else if (x.membership === "nonmember") {
          price += 150;
        }
      }
    }
  });
  return price;
};

const PlayersGame = (props) => {
  const router = useRouter();

  const {
    control,
    handleSubmit,
    formState: { errors },
    watch,
    setValue,
    reset,
  } = useForm();

  const [groupData, setGroupData] = useState();
  const [currentGolfers, setCurrentGolfers] = useState([]);
  const [added, setAdded] = useState();
  const [emailError, setEmailError] = useState(false);
  const [currentPrice, setCurrentPrice] = useState(0);
  const [deletedGolfer, setDeletedGolfer] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const grabGroupData = async () => {
      await axios
        .post(`${url}/api/group`, {
          data: {
            group_id: router.query.game,
          },
        })
        .then((res) => {
          setGroupData(res.data);
        });
    };

    if (router.query.game) {
      grabGroupData();
    }
  }, [router.query.game, url]);

  useEffect(() => {
    const grabAllGolfers = async () => {
      await axios
        .post(`${url}/api/findgolfersbygroupid`, {
          data: {
            group_id: router.query.game,
          },
        })
        .then((res) => {
          setCurrentGolfers(res.data);
          setCurrentPrice(calculatePrice(res.data));
        });
    };
    if (router.query.game) {
      grabAllGolfers();
    }
  }, [added, router.query.game, deletedGolfer, url]);

  const onSubmit = async (data) => {
    setLoading(true);
    await axios
      .post(`${url}/api/addplayer`, {
        data: {
          ...data,
          group_id: router.query.game,
        },
      })
      .then((res) => {
        setAdded(res.data);
      })
      .catch((err) => {
        setEmailError(true);
      });
    setLoading(false);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} id="form">
      <Container maxWidth="md">
        <Typography variant="h4" component="h1">
          {groupData?.game_type && groupData.game_type}
        </Typography>
        <p>
          Please list the players that are going to be paid for in this
          transaction.
        </p>
        <Box my={4}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography variant="h6" component="h1">
                Player
              </Typography>
            </Grid>

            <Grid item lg={6} xs={12}>
              <Controller
                name={`first_name`}
                control={control}
                defaultValue=""
                rules={{ required: true }}
                render={({ field }) => (
                  <TextField
                    error={errors[`first_name`]}
                    fullWidth
                    label="First Name"
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item lg={6} xs={12}>
              <Controller
                name={`last_name`}
                control={control}
                defaultValue=""
                rules={{ required: true }}
                render={({ field }) => (
                  <TextField
                    error={errors[`last_name`]}
                    fullWidth
                    label="Last Name"
                    {...field}
                  />
                )}
              />
            </Grid>
            {groupData?.game_type === "2-person-better-ball" && (
              <Grid item lg={3} xs={6}>
                <Controller
                  name={`ghin`}
                  control={control}
                  defaultValue=""
                  rules={{ maxLength: 7, minLength: 7 }}
                  render={({ field }) => (
                    <TextField
                      error={errors[`ghin`]}
                      fullWidth
                      label="GHIN"
                      {...field}
                    />
                  )}
                />
                {errors[`ghin`] && <p>Needs to be 7 digits long.</p>}
              </Grid>
            )}
            <Grid item lg={3} xs={6}>
              <Controller
                name={`phone`}
                control={control}
                defaultValue=""
                rules={{ required: true }}
                render={({ field }) => (
                  <TextField
                    error={errors[`phone`]}
                    fullWidth
                    label="Phone"
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item lg={6} xs={12}>
              <Controller
                name={`email`}
                control={control}
                defaultValue=""
                rules={{ required: true }}
                render={({ field }) => (
                  <>
                    <TextField
                      error={errors[`email`] || emailError}
                      fullWidth
                      label="Email"
                      {...field}
                    />
                    {emailError && <p>This email has already been used</p>}
                  </>
                )}
              />
            </Grid>
            {groupData?.game_type === "2-person-better-ball" && (
              <Grid item lg={6} xs={12}>
                <InputLabel>Tees</InputLabel>
                <Controller
                  name={`tees`}
                  control={control}
                  render={({ field }) => (
                    <Select defaultValue="none" fullWidth {...field}>
                      <MenuItem disabled={true} value="none">
                        <em>Select One</em>
                      </MenuItem>
                      <MenuItem value="redtail">Redtail - Men</MenuItem>
                      <MenuItem value="coyote">Coyote - Men</MenuItem>
                      <MenuItem value="sun">Sun - Ladies</MenuItem>
                    </Select>
                  )}
                />
              </Grid>
            )}
            <Grid item lg={6} xs={12}>
              <InputLabel>Shirt Size</InputLabel>
              <Controller
                name={`shirt_size`}
                control={control}
                render={({ field }) => (
                  <Select defaultValue="none" fullWidth {...field}>
                    <MenuItem disabled={true} value="none">
                      <em>Select One</em>
                    </MenuItem>
                    <MenuItem value="xs">xs</MenuItem>
                    <MenuItem value="s">s</MenuItem>
                    <MenuItem value="m">m</MenuItem>
                    <MenuItem value="l">l</MenuItem>
                    <MenuItem value="xl">xl</MenuItem>
                    <MenuItem value="xxl">xxl</MenuItem>
                  </Select>
                )}
              />
            </Grid>
            <Grid item lg={6} xs={12}>
              <InputLabel>Membership</InputLabel>
              <Controller
                name={`membership`}
                control={control}
                render={({ field }) => (
                  <Select defaultValue={0} fullWidth {...field}>
                    <MenuItem disabled={true} value={0}>
                      <em>Select One</em>
                    </MenuItem>
                    <MenuItem value="member">AC-Member $125</MenuItem>
                    <MenuItem value="nonmember">Non-Member $150</MenuItem>
                  </Select>
                )}
              />
            </Grid>
          </Grid>

          <Grid style={{ marginTop: "30px" }} item xs={12}>
            <input
              disabled={
                groupData?.game_type &&
                currentGolfers.length >=
                  parseInt(groupData.game_type.split("-")[0], 10)
                  ? true
                  : false
              }
              type="submit"
              value={loading ? "Loading..." : "Add Player"}
            />
            {groupData?.game_type &&
            currentGolfers.length >=
              parseInt(groupData.game_type.split("-")[0], 10) ? (
              <p>
                You cannot add anymore golfers. Please scroll down to checkout.
              </p>
            ) : null}
          </Grid>
          {currentGolfers && (
            <Typography
              style={{ marginTop: "30px" }}
              variant="h4"
              component="h4"
              gutterBottom
            >
              Current Golfers
            </Typography>
          )}
          <Grid container spacing={3}>
            {currentGolfers &&
              currentGolfers.map((x, i) => {
                return (
                  <Grid key={84764 + i} item lg={6} xs={12}>
                    <Card variant="outlined">
                      <CardContent>
                        <Grid item lg={6} xs={12}>
                          <Typography variant="h6" component="h6" gutterBottom>
                            {x.first_name} {x.last_name}
                          </Typography>
                          {groupData?.game_type === "2-person-better-ball" && (
                            <Typography color="textSecondary" gutterBottom>
                              Tees: {x.tees}
                            </Typography>
                          )}
                          <Typography color="textSecondary" gutterBottom>
                            Shirt Size: {x.shirt_size}
                          </Typography>
                          <Typography color="textSecondary" gutterBottom>
                            Membership: {x.membership}
                          </Typography>
                          <Typography color="textSecondary" gutterBottom>
                            Price: {x.membership === "member" ? "$125" : "$150"}
                          </Typography>
                        </Grid>
                        <Grid item lg={6} xs={12}>
                          {!x.paid ? (
                            <Button
                              style={{ margin: "20px 0" }}
                              size="small"
                              variant="contained"
                              onClick={async () => {
                                await axios
                                  .post(`${url}/api/deletegolfer`, {
                                    data: {
                                      golfer_id: x.golfer_id,
                                    },
                                  })
                                  .then((res) => setDeletedGolfer(res))
                                  .catch((err) => console.log(err));
                              }}
                            >
                              DELETE
                            </Button>
                          ) : (
                            <Button
                              style={{ margin: "20px 0" }}
                              size="small"
                              variant="contained"
                              disabled={true}
                            >
                              PAID
                            </Button>
                          )}
                        </Grid>
                      </CardContent>
                    </Card>
                  </Grid>
                );
              })}
          </Grid>

          <Button
            style={{ margin: "20px 0" }}
            disabled={
              groupData?.game_type &&
              currentGolfers.length >
                parseInt(groupData.game_type.split("-")[0], 10)
                ? true
                : currentPrice === 0
                ? true
                : false
            }
            variant="contained"
            onClick={() => {
              router.push({
                pathname: "/[game]/checkout",
                query: { game: router.query.game },
              });
            }}
          >
            Checkout - ${currentPrice}
          </Button>

          {groupData?.game_type &&
          currentGolfers.length >
            parseInt(groupData.game_type.split("-")[0], 10) ? (
            <p>
              Please delete some golfers. You are limited to{" "}
              {parseInt(groupData.game_type.split("-")[0], 10)}.
            </p>
          ) : null}
        </Box>
      </Container>
    </form>
  );
};

export default PlayersGame;
