import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Container from "@material-ui/core/Container";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Grid from "@material-ui/core/Grid";
import Switch from "@material-ui/core/Switch";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import url from "../../utils/baseUrl";

const calculatePrice = (golfers) => {
  let price = 0;
  golfers.map((x) => {
    if (!x.paid) {
      if (x.membership) {
        if (x.membership === "member") {
          price += 125;
        } else if (x.membership === "nonmember") {
          price += 150;
        }
      }
    }
  });
  return price;
};

const needToPay = (golfers) => {
  const id = [];
  golfers.map((x) => {
    if (!x.paid) {
      id.push(x.golfer_id);
    }
  });
  return id;
};

const CheckoutPage = () => {
  const router = useRouter();

  const [currentPrice, setCurrentPrice] = useState(0);
  const [currentGolfers, setCurrentGolfers] = useState([]);
  const [golfersIdThatNeedToPay, setGolfersIdThatNeedToPay] = useState([]);
  const [groupData, setGroupData] = useState();
  const [updateUserPaidStatus, setUpdateUserPaidStatus] = useState(false);

  const [paid, setPaid] = useState(false);
  const [error, setError] = useState(null);
  const [orderDetails, setOrderDetails] = useState();
  const [processingFee, setProcessingFee] = useState(false);
  const [renderPaypal, setRenderPaypal] = useState(false);
  const paypalRef = useRef();

  useEffect(() => {
    const grabAllGolfers = async () => {
      await axios
        .post(`${url}/api/findgolfersbygroupid`, {
          data: {
            group_id: router.query.game,
          },
        })
        .then((res) => {
          setCurrentGolfers(res.data);
          setCurrentPrice(calculatePrice(res.data));
          setGolfersIdThatNeedToPay(needToPay(res.data));
        })
        .finally(() => {
          setRenderPaypal(true);
        });
    };
    if (router.query.game) {
      grabAllGolfers();
    }
  }, [router.query.game, updateUserPaidStatus, url]);

  useEffect(() => {
    const grabGroupData = async () => {
      await axios
        .post(`${url}/api/group`, {
          data: {
            group_id: router.query.game,
          },
        })
        .then((res) => {
          setGroupData(res.data);
        });
    };

    if (router.query.game) {
      grabGroupData();
    }
  }, [router.query.game]);

  useEffect(() => {
    if (renderPaypal) {
      window.paypal
        .Buttons({
          createOrder: (data, actions) => {
            return actions.order.create({
              intent: "CAPTURE",
              purchase_units: [
                {
                  description: "Nevadans 4 Vets",
                  amount: {
                    currency_code: "USD",
                    value: currentPrice,
                  },
                },
              ],
            });
          },
          onApprove: async (data, actions) => {
            const order = await actions.order.capture();

            await axios
              .post(`${url}/api/updatepayment`, {
                data: {
                  group_id: router.query.game,
                  payment_id: order.id,
                  price_paid: order.purchase_units[0].amount.value,
                  golfers_id: golfersIdThatNeedToPay,
                },
              })
              .then((res) => {
                setOrderDetails(res.data);
                setUpdateUserPaidStatus(true);
              })
              .catch((err) => console.log(err));
            setPaid(true);
          },
          onError: (err) => {
            console.error(err, "HERE");
          },
        })
        .render(paypalRef.current);
    }
  }, [renderPaypal]);

  return (
    <Container maxWidth="md">
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h4" component="h1">
            Checkout page
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h6" component="h1">
            {groupData?.game_type && groupData.game_type}
          </Typography>
        </Grid>
        {currentGolfers.map((x, i) => {
          return (
            <Grid key={i} item xs={6}>
              <Card variant="outlined">
                <CardContent>
                  <Typography variant="h6" component="h6" gutterBottom>
                    {x.first_name} {x.last_name}{" "}
                    <span style={{ color: "green", fontSize: "14px" }}>
                      {x.paid && "(paid)"}
                    </span>
                  </Typography>
                  {groupData?.game_type === "2-person-better-ball" && (
                    <Typography color="textSecondary" gutterBottom>
                      Tees: {x.tees}
                    </Typography>
                  )}
                  <Typography color="textSecondary" gutterBottom>
                    Shirt Size: {x.shirt_size}
                  </Typography>
                  <Typography color="textSecondary" gutterBottom>
                    Membership: {x.membership}
                  </Typography>
                  <Typography color="textSecondary" gutterBottom>
                    Price: {x.membership === "member" ? "$125" : "$150"}
                  </Typography>
                  <Typography color="textSecondary" gutterBottom>
                    id: {x.golfer_id}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          );
        })}
        <Grid item xs={12}>
          {paid ? (
            <p>
              Payment was successful for ${orderDetails.price_paid}. Your
              confirmation number is {orderDetails.payment_id}. Your group id is{" "}
              {orderDetails.group_id}. Please keep this for your records.
            </p>
          ) : (
            <div>
              <Grid item xs={12}>
                {/* <FormControlLabel
                  control={
                    <Switch
                      checked={processingFee}
                      onClick={() => {
                        setProcessingFee(!processingFee);
                      }}
                      name="checkedB"
                    />
                  }
                  label="Would you like to cover the 3% + $0.30 processing fee?"
                /> */}
                <Typography variant="h6" component="h6" gutterBottom>
                  Total: $
                  {processingFee ? currentPrice * 1.03 + 0.3 : currentPrice}
                </Typography>
                <div
                  style={currentPrice === 0 ? { display: "none" } : {}}
                  ref={paypalRef}
                />
              </Grid>
            </div>
          )}
        </Grid>
      </Grid>
    </Container>
  );
};

export default CheckoutPage;
