import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import url from "../utils/baseUrl";

import Slider from "@material-ui/core/Slider";

const BallDrop = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();
  const router = useRouter();
  const onSubmit = async (data) => {
    console.log(data);
  };

  const [currentAmount, setCurrentAmount] = useState(50);
  const [remaining, setRemaining] = useState();
  const [numberOfBalls, setNumberOfBalls] = useState();

  function valuetext(value) {
    let num = value * 2,
      amt,
      bcnt;
    setCurrentAmount(value * 2);

    // 100 -> 15, 50 -> 7, 25 -> 3, 10 -> 1

    amt = Math.floor(num / 100);
    bcnt = amt * 15;
    num = num - amt * 100;

    amt = Math.floor(num / 50);
    bcnt = bcnt + amt * 7;
    num = num - amt * 50;

    amt = Math.floor(num / 25);
    bcnt = bcnt + amt * 3;
    num = num - amt * 25;

    amt = Math.floor(num / 10);
    bcnt = bcnt + amt;
    num = num - amt * 10;
    setNumberOfBalls(bcnt);
  }

  const marks = [
    {
      value: 5,
      label: "$10",
    },
    {
      value: 25,
      label: "$50",
    },
    {
      value: 50,
      label: "$100",
    },
    {
      value: 100,
      label: "$200",
    },
  ];

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Container maxWidth="md">
        <Box my={4}>
          <Grid container spacing={3}>
            <Grid style={{ marginBottom: "50px" }} item xs={12}>
              <Typography variant="h6" component="h1">
                Ball Drop
              </Typography>
              <p>The more you buy the more of a discount you recieve.</p>
            </Grid>
            <Grid item xs={12}>
              <Typography id="discrete-slider-custom" gutterBottom>
                You have selected ${currentAmount}
              </Typography>
              <Slider
                defaultValue={50}
                getAriaValueText={valuetext}
                aria-labelledby="discrete-slider-custom"
                step={2.5}
                valueLabelDisplay="auto"
                marks={marks}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" component="h1">
                You can purchase {numberOfBalls} balls
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </form>
  );
};

export default BallDrop;
