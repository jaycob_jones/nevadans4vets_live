const url =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3000"
    : "https://register.nevadans4vets.org";

export default url;
