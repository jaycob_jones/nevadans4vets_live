import React from "react";

const Header = () => {
  return (
    <div>
      <div
        style={{
          fontFamily: '"Yeseva One", cursive',
          fontSize: "60px",
          textAlign: "center",
          textTransform: "uppercase",
          margin: "30px 0",
        }}
      >
        Nevadans 4 Vets
      </div>
    </div>
  );
};

export default Header;
