-- CreateTable
CREATE TABLE "Orders" (
    "id" SERIAL NOT NULL,
    "game" VARCHAR(255),
    "createdat" DATE NOT NULL DEFAULT CURRENT_DATE,
    "cover_processing" VARCHAR(255),
    "email_player_1" VARCHAR(255),
    "firstname_player_1" VARCHAR(255),
    "ghin_player_1" VARCHAR(255),
    "lastname_player_1" VARCHAR(255),
    "membership_player_1" INTEGER,
    "phone_player_1" VARCHAR(255),
    "shirt_player_1" VARCHAR(255),
    "tees_player_1" VARCHAR(255),
    "email_player_2" VARCHAR(255),
    "firstname_player_2" VARCHAR(255),
    "ghin_player_2" VARCHAR(255),
    "lastname_player_2" VARCHAR(255),
    "membership_player_2" INTEGER,
    "phone_player_2" VARCHAR(255),
    "shirt_player_2" VARCHAR(255),
    "tees_player_2" VARCHAR(255),
    "price" VARCHAR(255),

    PRIMARY KEY ("id")
);
