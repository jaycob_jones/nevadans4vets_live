/*
  Warnings:

  - You are about to drop the `Orders` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "Orders";

-- CreateTable
CREATE TABLE "golfer" (
    "golfer_id" SERIAL NOT NULL,
    "group_id" INTEGER NOT NULL,
    "created_at" DATE NOT NULL DEFAULT CURRENT_DATE,
    "email" TEXT NOT NULL,
    "first_name" VARCHAR(255),
    "last_name" VARCHAR(255),
    "ghin" VARCHAR(255),
    "membership" VARCHAR(255),
    "phone" VARCHAR(255),
    "shirt_size" VARCHAR(255),
    "tees" VARCHAR(255),

    PRIMARY KEY ("golfer_id")
);

-- CreateTable
CREATE TABLE "groups" (
    "group_id" SERIAL NOT NULL,
    "game_type" VARCHAR(255),
    "created_at" DATE NOT NULL DEFAULT CURRENT_DATE,
    "cover_processing" VARCHAR(255),
    "price_paid" INTEGER,
    "payment_id" VARCHAR(255),

    PRIMARY KEY ("group_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "golfer.email_unique" ON "golfer"("email");

-- AddForeignKey
ALTER TABLE "golfer" ADD FOREIGN KEY ("group_id") REFERENCES "groups"("group_id") ON DELETE CASCADE ON UPDATE CASCADE;
