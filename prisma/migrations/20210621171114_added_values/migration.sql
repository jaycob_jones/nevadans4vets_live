-- AlterTable
ALTER TABLE "golfer" ADD COLUMN     "paid" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "transaction_id" VARCHAR(255);
